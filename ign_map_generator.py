import concurrent.futures
import io
import math
import os
from urllib.parse import urlencode

import requests

from PIL import Image


def get_map_url(row=22672, col=33263, level=16):
    root = "https://wxs.ign.fr/an7nvfzojv5wa96dsga5nk8w/geoportail/wmts"
    query_params = dict(
        layer="GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD",
        style="normal",
        tilematrixset="PM",
        Service="WMTS",
        Request="GetTile",
        Version="1.0.0",
        Format="image/jpeg",
        TileMatrix=level,
        TileCol=col,
        TileRow=row,
    )
    return f"{root}?" + urlencode(query_params)


def get_urls(row0, rown, col0, coln, level):
    return [
        (row_idx, col_idx, get_map_url(row, col, level))
        for row_idx, row in enumerate(range(row0, rown))
        for col_idx, col in enumerate(range(col0, coln))
    ]


def download_image(url, timeout):
    response = requests.get(url, timeout=timeout, stream=True)

    if response.status_code == 200:
        f = io.BytesIO()
        # with open(os.path.join(path, f'{row}-{col}.jpg'), 'wb') as f:
        for chunk in response:
            f.write(chunk)
    else:
        raise Exception(f"{row}-{col}")

    return f


def get_x(lon):
    lon_rad = lon * math.pi / 180
    a = 6378137.0
    X0 = -20037508
    return a * lon_rad - X0


def get_y(lat):

    lat_rad = lat * math.pi / 180
    a = 6378137.0
    Y0 = 20037508
    return Y0 - a * math.log(math.tan(lat_rad / 2 + math.pi / 4))


def get_layer_scale(level):
    # in m/px
    # see https://depot.ign.fr/geoportail/api/develop/tech-docs-js/webmaster/layers.html
    # 0	156543.0339280410	1.40625000000000000000	1 : 559082264
    # 1	78271.5169640205	0.70312500000000000000	1 : 279541132
    # 2	39135.7584820102	0.35156250000000000000	1 : 139770566
    # 3	19567.8792410051	0.17578125000000000000	1 : 69885283
    # 4	9783.9396205026	0.08789062500000000000	1 : 34942642
    # 5	4891.9698102513	0.04394531250000000000	1 : 17471321
    # 6	2445.9849051256	0.02197265625000000000	1 : 8735660
    # 7	1222.9924525628	0.01098632812500000000	1 : 4367830
    # 8	611.4962262814	0.00549316406250000000	1 : 2183915
    # 9	305.7481131407	0.00274658203125000000	1 : 1091958
    # 10	152.8740565704	0.00137329101562500000	1 : 545979
    # 11	76.4370282852	0.00068664550781250000	1 : 272989
    # 12	38.2185141426	0.00034332275390625000	1 : 136495
    # 13	19.1092570713	0.00017166137695312500	1 : 68247
    # 14	9.5546285356	0.00008583068847656250	1 : 34124
    # 15	4.7773142678	0.00004291534423828120	1 : 17062
    # 16	2.3886571339	0.00002145767211914060	1 : 8531
    # 17	1.1943285670	0.00001072883605957030	1 : 4265
    # 18	0.5971642835	0.00000536441802978516	1 : 2133
    # 19	0.2985821417	0.00000268220901489258	1 : 1066
    # 20	0.1492910709	0.00000134110450744629	1 : 533
    # 21	0.0746455354	0.00000067055225372315	1 : 267
    layers_scaling = [
        156543.0339280410,
        78271.5169640205,
        39135.7584820102,
        19567.8792410051,
        9783.9396205026,
        4891.9698102513,
        2445.9849051256,
        1222.9924525628,
        611.4962262814,
        305.7481131407,
        152.8740565704,
        76.4370282852,
        38.2185141426,
        19.1092570713,
        9.5546285356,
        4.7773142678,
        2.3886571339,
        1.1943285670,
        0.5971642835,
        0.2985821417,
        0.1492910709,
        0.0746455354,
    ]
    return layers_scaling[level]


def compute_image(
    lat_min, lon_min, lat_max, lon_max, level=16, max_images=2500, image_path=None
):
    # for reference frame computation see:
    # https://depot.ign.fr/geoportail/api/develop/tech-docs-js/developpeur/wmts.html
    #
    # a: equatorial radius (semi-major axis) of the ellipsoid, is worth 6378137.0 meters
    # X= a * lon
    # Y= a * ln(tan(lat/2 + pi/4))

    # NB: the above formula for computing the ordinate is equivalent to this one:

    # Y= a/2 * ln((1+sin(lat))/(1-sin(lat))

    tile_size_px = 256

    tile_size_meter = tile_size_px * get_layer_scale(level)

    col0 = int(math.floor(get_x(lon_min) / tile_size_meter))
    coln = int(math.ceil(get_x(lon_max) / tile_size_meter))
    row0 = int(math.floor(get_y(lat_min) / tile_size_meter))
    rown = int(math.ceil(get_y(lat_max) / tile_size_meter))

    print("Loading mini images between...")
    print((lat_min, lon_min), "and", (lat_max, lon_max), "at level", level)
    print("rows", row0, rown)
    print("cols", col0, coln)

    urls = get_urls(row0, rown, col0, coln, level)

    if len(urls) > max_images:
        raise Exception(f"Too many images to download, max set to {max_images}")

    with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:

        future_to_row_col_index = {
            executor.submit(download_image, url, 10): (r, c) for (r, c, url) in urls
        }
        resp_err = 0

        resp_ok = 0

        file_list = []

        for future in concurrent.futures.as_completed(future_to_row_col_index):
            (r, c) = future_to_row_col_index[future]
            try:
                io_file = future.result()
                result = (r, c, io_file)
                file_list.append(result)
            except Exception as exc:
                resp_err = resp_err + 1
            else:
                resp_ok = resp_ok + 1

    if resp_ok != len(urls):
        raise Exception(
            f"Not the expected number of OK response ('{resp_ok}' != '{len(urls)}')"
        )

    print(
        "Final image size", tile_size_px * (rown - row0), tile_size_px * (coln - col0)
    )

    full_image = Image.new(
        "RGB", (tile_size_px * (coln - col0), tile_size_px * (rown - row0))
    )

    for result in file_list:
        r, c, f = result
        mini_img = Image.open(f)
        print("writing image slice", r, c)
        full_image.paste(mini_img, (c * tile_size_px, r * tile_size_px))

    if image_path is None:
        image_path = f"full_image_r{row0}-{rown}_c{col0}-{coln}_lvl{level}.jpg"
    full_image.save(image_path)


if __name__ == "__main__":
    import argparse

    class SmartFormatter(argparse.HelpFormatter):
        def _split_lines(self, text, width):
            if text.startswith("R|"):
                return text[2:].splitlines()
            # this is the RawTextHelpFormatter._split_lines
            return argparse.HelpFormatter._split_lines(self, text, width)

    parser = argparse.ArgumentParser(
        description="Download and merge IGN maps using GPS coordinates",
        formatter_class=SmartFormatter,
    )
    parser.add_argument(
        "top_left_coordinates",
        help='The coordinates of the top-left corner, example: "48.812181, 2.227066"',
    )
    parser.add_argument(
        "bot_right_coordinates",
        help='The coordinates of the bot-right corner, example: "48.812181, 2.227066"',
    )

    parser.add_argument(
        "level",
        type=int,
        help="R|The level of detail for the map: \n"
        "0	1 : 559082264\n"
        "1	1 : 279541132\n"
        "2	1 : 139770566\n"
        "3	1 : 69885283\n"
        "4	1 : 34942642\n"
        "5	1 : 17471321\n"
        "6	1 : 8735660\n"
        "7	1 : 4367830\n"
        "8	1 : 2183915\n"
        "9	1 : 1091958\n"
        "10	1 : 545979\n"
        "11	1 : 272989\n"
        "12	1 : 136495\n"
        "13	1 : 68247\n"
        "14	1 : 34124\n"
        "15	1 : 17062\n"
        "16	1 : 8531\n"
        "17	1 : 4265\n"
        "18	1 : 2133\n"
        "19	1 : 1066\n"
        "20	1 : 533\n"
        "21	1 : 267\n",
    )

    parser.add_argument(
        "--max_images",
        default=2500,
        help="Max images the software can download",
        type=int,
    )

    parser.add_argument(
        "--image_path", default=None, help="Path of the merged image (.jpg)",
    )

    args = parser.parse_args()

    coord_min = [float(value) for value in args.top_left_coordinates.split(", ")]
    coord_max = [float(value) for value in args.bot_right_coordinates.split(", ")]
    compute_image(
        coord_min[0],
        coord_min[1],
        coord_max[0],
        coord_max[1],
        level=args.level,
        max_images=args.max_images,
        image_path=args.image_path,
    )
