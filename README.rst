Install
~~~~~~~~~

We use `poetry`_ :
    poetry install

Usage
~~~~~~

As soon as the package is installed, you can display the help message with :
    python -m ign_map_generator --help
    

.. _poetry: https://python-poetry.org/